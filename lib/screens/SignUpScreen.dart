import 'package:ecommerce/services/AppleAuth.dart';
import 'package:ecommerce/services/FacebookAuth.dart';
import 'package:ecommerce/services/GoogleAuth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../utils/constants.dart';
import '../widgets/AuthScreenTextField.dart';
import '../widgets/AuthScreenButton.dart';

class SignUpScreen extends StatelessWidget {
  void buttonPressedHandler() {
    print('Button has been pressed');
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(deviceSize.width * 0.05),
          margin: EdgeInsets.symmetric(horizontal: deviceSize.width * 0.05),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(13.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey[300],
                offset: Offset(0, deviceSize.height * 0.05),
                blurRadius: 80.0,
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              AuthScreenTextField(
                labelText: 'EMAIL',
                icon: Icons.mail_outline,
              ),
              SizedBox(
                height: 8.0,
              ),
              AuthScreenTextField(
                labelText: 'USERNAME',
                icon: Icons.person_outline,
              ),
              SizedBox(
                height: 8.0,
              ),
              AuthScreenTextField(
                labelText: 'PASSWORD',
                icon: Icons.lock_open,
              ),
            ],
          ),
        ),

        SizedBox(
          height: deviceSize.width * 0.05,
        ),

        // Creating the AuthScreen button

        AuthScreenButton(
          buttonText: 'SIGN UP',
          onPressed: buttonPressedHandler,
        ),
        SizedBox(
          height: deviceSize.width * 0.05,
        ),

        Text(
          'OR',
          style: TextStyle(
            fontFamily: 'Neusa Next Std',
            color: kUnselectedColor,
            fontSize: 20.0,
          ),
        ),

        SizedBox(
          height: deviceSize.width * 0.05,
        ),

        // Creating the AuthScreen button
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Spacer(),
            AppleAuth(),
            GoogleAuth(),
            FacebookAuth(),
            Spacer(),
          ],
        ),

        // Creating the Social Media Authetication

        SizedBox(
          height: deviceSize.width * 0.08,
        ),
        // Creating the legal text

        Text(
          'By creating an account, you agree to our',
          style: TextStyle(
            color: kTextColor2,
            fontFamily: 'Neusa Next Std',
            fontWeight: FontWeight.w100,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Terms of Service',
              style: TextStyle(
                color: kPrimaryColor,
                fontFamily: 'Neusa Next Std',
                fontWeight: FontWeight.w100,
              ),
            ),
            Text(
              ' and ',
              style: TextStyle(
                color: kTextColor2,
                fontFamily: 'Neusa Next Std',
                fontWeight: FontWeight.w100,
              ),
            ),
            Text(
              'Privacy Policy',
              style: TextStyle(
                color: kPrimaryColor,
                fontFamily: 'Neusa Next Std',
                fontWeight: FontWeight.w100,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
