import 'package:flutter/material.dart';
import '../utils/constants.dart';
import '../widgets/AuthScreenTab.dart';
import '../screens/SignUpScreen.dart';
import '../screens/LogInScreen.dart';
import '../screens/ForgotPasswordScreen.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: 3,
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: kBackgroundColor,
      body: Column(
        children: <Widget>[
          SizedBox(
            height: deviceSize.height * 0.09,
          ),
          Padding(
            padding: EdgeInsets.only(left: deviceSize.width * 0.05),

            // Creating the TabBar
            child: TabBar(
              controller: _tabController,
              indicatorColor: Colors.transparent,
              isScrollable: true,
              labelColor: kSelectedColor,
              unselectedLabelColor: kUnselectedColor,
              labelPadding: EdgeInsets.only(right: deviceSize.width * 0.08),
              tabs: <Widget>[
                AuthScreenTab(
                  tabText: 'Sign Up',
                ),
                AuthScreenTab(
                  tabText: 'Log In',
                ),
                AuthScreenTab(
                  tabText: 'Forgot Password',
                ),
              ],
            ),
          ),
          SizedBox(
            height: deviceSize.height * 0.04,
          ),

          // Creating the TabView for each tabs
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: <Widget>[
                SignUpScreen(),
                LogInScreen(),
                ForgotPasswordScreen(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
