import 'package:flutter/material.dart';
import '../utils/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../widgets/InternalButton.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int bottomNavigationBarItemIndex = 0;

  @override
  Widget build(BuildContext context) {
    final Size deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            onPressed: () {
              print('Message IconButton pressed');
            },
            icon: Icon(
              Icons.chat_bubble_outline,
              color: kSelectedColor,
              size: 30.0,
            ),
            padding: EdgeInsets.symmetric(
              horizontal: 18.0,
            ),
          ),
          IconButton(
            onPressed: () {
              print('Notification IconButton pressed');
            },
            icon: Icon(
              Icons.notifications_none,
              color: kSelectedColor,
              size: 30.0,
            ),
            padding: EdgeInsets.symmetric(
              horizontal: 18.0,
            ),
          ),
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: <Color>[
              kBackgroundColor,
              Colors.white,
            ],
            tileMode: TileMode.clamp,
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(left: deviceSize.width * 0.05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Categories",
                style: TextStyle(
                  color: kSelectedColor,
                  fontFamily: 'Neusa Next Std',
                  fontWeight: FontWeight.w600,
                  fontSize: 35,
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.only(right: deviceSize.width * 0.05, top: 12.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          width: deviceSize.width * 0.1875,
                          height: deviceSize.width * 0.1875,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              colors: <Color>[
                                kClothesBottomGradientColor,
                                kClothesTopGradientColor,
                              ],
                              tileMode: TileMode.clamp,
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: kClothesBottomGradientColor,
                                offset: Offset(0, deviceSize.height * 0.05),
                                blurRadius: 20.0,
                                spreadRadius: -13,
                              ),
                            ],
                            shape: BoxShape.circle,
                          ),
                          child: RaisedButton(
                            onPressed: () {
                              print('Clothes button pressed');
                            },
                            color: Colors.transparent,
                            elevation: 0.0,
                            highlightElevation: 0.0,
                            shape: CircleBorder(),
                            child: SvgPicture.asset(
                              'lib/assets/icons/clothes.svg',
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 9.0),
                          child: Text(
                            'Apparel',
                            style: TextStyle(
                              fontFamily: 'Neusa Next Std',
                              fontWeight: FontWeight.w300,
                              fontSize: 17.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          width: deviceSize.width * 0.1875,
                          height: deviceSize.width * 0.1875,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              colors: <Color>[
                                kBeautyBottomGradientColor,
                                kBeautyTopGradientColor,
                              ],
                              tileMode: TileMode.clamp,
                            ),
                            boxShadow: [
                              BoxShadow(
                                  color: kBeautyBottomGradientColor,
                                  offset: Offset(0, deviceSize.height * 0.05),
                                  blurRadius: 20.0,
                                  spreadRadius: -13),
                            ],
                            shape: BoxShape.circle,
                          ),
                          child: RaisedButton(
                            onPressed: () {
                              print('Beauty button pressed');
                            },
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            color: Colors.transparent,
                            elevation: 0.0,
                            highlightElevation: 0.0,
                            shape: CircleBorder(),
                            child: SvgPicture.asset(
                              'lib/assets/icons/beauty.svg',
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 9.0),
                          child: Text(
                            'Beauty',
                            style: TextStyle(
                              fontFamily: 'Neusa Next Std',
                              fontWeight: FontWeight.w300,
                              fontSize: 17.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          width: deviceSize.width * 0.1875,
                          height: deviceSize.width * 0.1875,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              colors: <Color>[
                                kShoesBottomGradientColor,
                                kShoesTopGradientColor,
                              ],
                              tileMode: TileMode.clamp,
                            ),
                            boxShadow: [
                              BoxShadow(
                                  color: kShoesBottomGradientColor,
                                  offset: Offset(0, deviceSize.height * 0.05),
                                  blurRadius: 20.0,
                                  spreadRadius: -13),
                            ],
                            shape: BoxShape.circle,
                          ),
                          child: RaisedButton(
                            onPressed: () {
                              print('Shoes button pressed');
                            },
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            color: Colors.transparent,
                            elevation: 0.0,
                            highlightElevation: 0.0,
                            shape: CircleBorder(),
                            child: SvgPicture.asset(
                              'lib/assets/icons/shoes.svg',
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 9.0),
                          child: Text(
                            'Shoes',
                            style: TextStyle(
                              fontFamily: 'Neusa Next Std',
                              fontWeight: FontWeight.w300,
                              fontSize: 17.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          width: deviceSize.width * 0.1875,
                          height: deviceSize.width * 0.1875,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: kTextColor,
                                  offset: Offset(0, deviceSize.height * 0.05),
                                  blurRadius: 20.0,
                                  spreadRadius: -13),
                            ],
                            shape: BoxShape.circle,
                          ),
                          child: RaisedButton(
                            onPressed: () {
                              print('See All button pressed');
                            },
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            color: Colors.transparent,
                            elevation: 0.0,
                            highlightElevation: 0.0,
                            shape: CircleBorder(),
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: kPrimaryColor,
                              size: 30.0,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 9.0),
                          child: Text(
                            'See All',
                            style: TextStyle(
                              fontFamily: 'Neusa Next Std',
                              fontWeight: FontWeight.w300,
                              fontSize: 17.0,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                'Latest',
                style: TextStyle(
                  color: kSelectedColor,
                  fontFamily: 'Neusa Next Std',
                  fontWeight: FontWeight.w600,
                  fontSize: 35,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 8.0,
                ),
                height: deviceSize.height * 0.25,

                // ListView for the Latest section

                child: ListView(
                  scrollDirection: Axis.horizontal,

                  // Children for the ListView
                  children: <Widget>[
                    // 1st child
                    Container(
                      margin: EdgeInsets.only(
                        right: 15.0,
                      ),
                      width: deviceSize.width * 0.9,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage(
                            'lib/assets/images/summerClothing.jpg',
                          ),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'For all your\nsummer clothing\nneeds',
                              style: TextStyle(
                                fontFamily: 'Neusa Next Std',
                                fontWeight: FontWeight.w300,
                                color: Colors.white,
                                fontSize: 22.0,
                              ),
                            ),
                            InternalButton(
                              buttonText: 'SEE MORE',
                              onPressed: () {
                                print('SEE MORE Internal button pressed');
                              },
                            ),
                          ],
                        ),
                      ),
                    ),

                    // 2nd child
                    Container(
                      margin: EdgeInsets.only(
                        right: 15.0,
                      ),
                      width: deviceSize.width * 0.9,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage(
                            'lib/assets/images/fashion.jpg',
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              // Product Tile Row
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // 1st Item
                  Container(
                    width: deviceSize.width * 0.28,
                    height: 213,
                    margin: EdgeInsets.only(
                      top: deviceSize.width * 0.03,
                      right: deviceSize.width * 0.03,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(
                        10.0,
                      ),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: kTextColor2,
                          blurRadius: 20.0,
                          spreadRadius: -15,
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 12.0,
                        bottom: 12.0,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Spacer(),
                          Image.asset(
                            'lib/assets/images/ankleBoots.png',
                            width: 90.0,
                            height: 90.0,
                          ),
                          Spacer(),
                          RichText(
                            text: TextSpan(
                              text: 'Ankle Boots\n',
                              style: TextStyle(
                                fontFamily: 'Neusa Next Std',
                                fontWeight: FontWeight.w300,
                                color: kSelectedColor,
                                fontSize: 20.0,
                                letterSpacing: 0.7,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: '\$49.99',
                                  style: TextStyle(
                                    fontFamily: 'Neusa Next Std',
                                    fontWeight: FontWeight.w700,
                                    fontSize: 15.0,
                                    letterSpacing: 0.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  // 2nd Item
                  Container(
                    width: deviceSize.width * 0.28,
                    height: 213,
                    margin: EdgeInsets.only(
                      top: deviceSize.width * 0.03,
                      right: deviceSize.width * 0.03,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(
                        10.0,
                      ),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: kTextColor2,
                          blurRadius: 20.0,
                          spreadRadius: -15,
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 12.0,
                        bottom: 12.0,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Spacer(),
                          Image.asset(
                            'lib/assets/images/backpack.png',
                            width: 90.0,
                            height: 90.0,
                          ),
                          Spacer(),
                          RichText(
                            text: TextSpan(
                              text: 'Backpack\n',
                              style: TextStyle(
                                fontFamily: 'Neusa Next Std',
                                fontWeight: FontWeight.w300,
                                color: kSelectedColor,
                                fontSize: 20.0,
                                letterSpacing: 0.7,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: '\$20.58',
                                  style: TextStyle(
                                    fontFamily: 'Neusa Next Std',
                                    fontWeight: FontWeight.w700,
                                    fontSize: 15.0,
                                    letterSpacing: 0.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  // 3rd Item
                  Container(
                    width: deviceSize.width * 0.28,
                    height: 213,
                    margin: EdgeInsets.only(
                      top: deviceSize.width * 0.03,
                      right: deviceSize.width * 0.03,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(
                        10.0,
                      ),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: kTextColor2,
                          blurRadius: 20.0,
                          spreadRadius: -15,
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 12.0,
                        bottom: 12.0,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Spacer(),
                          Image.asset(
                            'lib/assets/images/redScarf.png',
                            width: 90.0,
                            height: 90.0,
                          ),
                          Spacer(),
                          RichText(
                            text: TextSpan(
                              text: 'Red Scarf\n',
                              style: TextStyle(
                                fontFamily: 'Neusa Next Std',
                                fontWeight: FontWeight.w300,
                                color: kSelectedColor,
                                fontSize: 20.0,
                                letterSpacing: 0.7,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: '\$11.00',
                                  style: TextStyle(
                                    fontFamily: 'Neusa Next Std',
                                    fontWeight: FontWeight.w700,
                                    fontSize: 15.0,
                                    letterSpacing: 0.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),

      // BottomNavigationBar
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: kPrimaryColor,
        unselectedItemColor: kTextColor2,
        showUnselectedLabels: true,
        selectedLabelStyle: TextStyle(
          fontFamily: 'Neusa Next Std',
          fontWeight: FontWeight.w300,
          fontSize: 14.0,
        ),
        unselectedLabelStyle: TextStyle(
          fontFamily: 'Neusa Next Std',
          fontWeight: FontWeight.w300,
          fontSize: 14.0,
        ),
        currentIndex: bottomNavigationBarItemIndex,
        iconSize: 27.0,
        onTap: (index) {
          setState(
            () {
              bottomNavigationBarItemIndex = index;
            },
          );
        },
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            title: Text(
              'Home',
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.search,
            ),
            title: Text(
              'Search',
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.add_shopping_cart,
            ),
            title: Text(
              'Cart',
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.perm_identity,
            ),
            title: Text(
              'Profile',
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.menu,
            ),
            title: Text(
              'More',
            ),
          ),
        ],
      ),
    );
  }
}
