import 'package:ecommerce/services/AppleAuth.dart';
import 'package:ecommerce/services/FacebookAuth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../utils/constants.dart';
import '../widgets/AuthScreenTextField.dart';
import '../widgets/AuthScreenButton.dart';
import '../services/GoogleAuth.dart';

class LogInScreen extends StatelessWidget {
  void buttonPressedHandler() {
    print('Button has been pressed');
  }

  @override
  Widget build(BuildContext context) {
    final Size deviceSize = MediaQuery.of(context).size;

    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(deviceSize.width * 0.05),
          margin: EdgeInsets.symmetric(horizontal: deviceSize.width * 0.05),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey[300],
                offset: Offset(0, deviceSize.height * 0.05),
                blurRadius: 80.0,
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              AuthScreenTextField(
                labelText: 'USERNAME / EMAIL',
                icon: Icons.perm_identity,
              ),
              SizedBox(
                height: 8.0,
              ),
              AuthScreenTextField(
                labelText: 'PASSWORD',
                icon: Icons.lock_open,
              ),
            ],
          ),
        ),

        SizedBox(
          height: deviceSize.width * 0.05,
        ),

        AuthScreenButton(
          buttonText: 'LOG IN',
          onPressed: buttonPressedHandler,
        ),

        SizedBox(
          height: deviceSize.width * 0.05,
        ),

        Text(
          'OR',
          style: TextStyle(
            fontFamily: 'Neusa Next Std',
            color: kUnselectedColor,
            fontSize: 20.0,
          ),
        ),

        SizedBox(
          height: deviceSize.width * 0.05,
        ),

        // Creating the AuthScreen button
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Spacer(),
            AppleAuth(),
            GoogleAuth(),
            FacebookAuth(),
            Spacer(),
          ],
        ),

        // Creating the Social Media Authetication

        SizedBox(
          height: deviceSize.width * 0.08,
        ),

        // Creating the legal text

        Text(
          'Don’t have an account? Swipe right to',
          style: TextStyle(
            color: kTextColor2,
            fontFamily: 'Neusa Next Std',
            fontWeight: FontWeight.w100,
          ),
        ),
        Text(
          'create a new account.',
          style: TextStyle(
            color: kPrimaryColor,
            fontFamily: 'Neusa Next Std',
            fontWeight: FontWeight.w100,
          ),
        ),
      ],
    );
  }
}
