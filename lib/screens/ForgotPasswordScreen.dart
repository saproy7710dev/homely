import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../utils/constants.dart';
import '../widgets/AuthScreenTextField.dart';
import '../widgets/AuthScreenButton.dart';

class ForgotPasswordScreen extends StatelessWidget {
  void buttonPressedHandler() {
    print('Button has been pressed');
  }

  @override
  Widget build(BuildContext context) {
    final Size deviceSize = MediaQuery.of(context).size;

    return Column(
      children: <Widget>[
        Text(
          'Enter the email address you used to create',
          style: TextStyle(
            color: kTextColor2,
            fontFamily: 'Neusa Next Std',
            fontWeight: FontWeight.w300,
            fontSize: 18.0,
          ),
        ),
        Text(
          'your account and we will email you a link to',
          style: TextStyle(
            color: kTextColor2,
            fontFamily: 'Neusa Next Std',
            fontWeight: FontWeight.w300,
            fontSize: 18.0,
          ),
        ),
        Text(
          'reset your password',
          style: TextStyle(
            color: kTextColor2,
            fontFamily: 'Neusa Next Std',
            fontWeight: FontWeight.w300,
            fontSize: 18.0,
          ),
        ),
        SizedBox(
          height: deviceSize.width * 0.09,
        ),
        Container(
          padding: EdgeInsets.all(deviceSize.width * 0.05),
          margin: EdgeInsets.symmetric(horizontal: deviceSize.width * 0.05),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey[300],
                offset: Offset(0, deviceSize.height * 0.05),
                blurRadius: 80.0,
              ),
            ],
          ),
          child: AuthScreenTextField(
            labelText: 'EMAIL',
            icon: Icons.mail_outline,
          ),
        ),

        SizedBox(height: deviceSize.width * 0.05),
        // Creating the AuthScreen button

        AuthScreenButton(
          buttonText: 'SEND EMAIL',
          onPressed: buttonPressedHandler,
        ),
      ],
    );
  }
}
