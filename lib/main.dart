import 'package:flutter/material.dart';
import 'screens/AuthScreen.dart';
import 'screens/HomeScreen.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Theme(
        data: ThemeData(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
        child: AuthScreen(),
      ),
    ),
  );
}
