import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import '../utils/constants.dart';

class AuthScreenButton extends StatelessWidget {
  AuthScreenButton({@required this.buttonText, this.onPressed});

  final String buttonText;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    final Size deviceSize = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(
        horizontal: deviceSize.width * 0.05,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 10.0,
        vertical: 3.0,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(
          28.0,
        ),
        color: kPrimaryColor,
        boxShadow: [
          BoxShadow(
            color: kPrimaryColor,
            offset: Offset(0, deviceSize.height * 0.017),
            blurRadius: 20.0,
            spreadRadius: -15,
          ),
        ],
      ),
      child: RawMaterialButton(
        onPressed: onPressed,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Spacer(),
            Text(
              buttonText,
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Neusa Next Std',
                fontWeight: FontWeight.w700,
                letterSpacing: 2.0,
              ),
            ),
            Spacer(),
            Container(
              width: 36.0,
              height: 36.0,
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
              ),
              child: Icon(
                Icons.arrow_forward_ios,
                color: kPrimaryColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
