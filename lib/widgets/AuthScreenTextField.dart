import 'package:ecommerce/utils/constants.dart';
import 'package:flutter/material.dart';

class AuthScreenTextField extends StatelessWidget {
  AuthScreenTextField({this.labelText, this.icon});
  final String labelText;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: labelText == 'PASSWORD' ? true : false,
      keyboardType: labelText == 'EMAIL'
          ? TextInputType.emailAddress
          : TextInputType.text,
      cursorColor: kPrimaryColor,
      cursorWidth: 2.7,
      decoration: InputDecoration(
        border: InputBorder.none,
        icon: Icon(
          icon,
          color: kSelectedColor,
          size: 26.0,
        ),
        contentPadding: EdgeInsets.only(right: 25.0),
        labelText: labelText,
        labelStyle: TextStyle(
          fontFamily: 'Neusa Next Std',
          fontWeight: FontWeight.w300,
          letterSpacing: 2.0,
          color: kTextColor,
        ),
        hintStyle: TextStyle(
          fontFamily: 'Neusa Next Std',
          fontWeight: FontWeight.w300,
          letterSpacing: 2.0,
          color: kTextColor,
        ),
      ),
    );
  }
}
