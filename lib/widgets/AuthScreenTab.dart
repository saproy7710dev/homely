import 'package:flutter/material.dart';

class AuthScreenTab extends StatelessWidget {
  AuthScreenTab({this.tabText});
  final String tabText;

  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Text(
        tabText,
        style: TextStyle(
          fontSize: 38.0,
          fontFamily: 'Neusa Next Std',
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
