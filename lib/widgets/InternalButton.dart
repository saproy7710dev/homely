import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import '../utils/constants.dart';

class InternalButton extends StatelessWidget {
  InternalButton({@required this.buttonText, this.onPressed});

  final String buttonText;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    final Size deviceSize = MediaQuery.of(context).size;

    return Container(
      width: deviceSize.width * .35,
      padding: EdgeInsets.symmetric(
        horizontal: 6.0,
        // vertical: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(
          28.0,
        ),
        color: Colors.white,
      ),
      child: RawMaterialButton(
        onPressed: onPressed,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Spacer(),
            Text(
              buttonText,
              style: TextStyle(
                color: kTextColor2,
                fontFamily: 'Neusa Next Std',
                letterSpacing: 1.5,
                fontSize: 15.0,
              ),
            ),
            Spacer(),
            Container(
              width: 35.0,
              height: 35.0,
              decoration: BoxDecoration(
                color: kPrimaryColor,
                shape: BoxShape.circle,
              ),
              child: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
                size: 20.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
