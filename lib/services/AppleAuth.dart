import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppleAuth extends StatefulWidget {
  @override
  _AppleAuthState createState() => _AppleAuthState();
}

class _AppleAuthState extends State<AppleAuth> {
  handleSignIn() {
    print('Apple Sign In Button clicked');
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: SvgPicture.asset('lib/assets/icons/apple.svg'),
      onPressed: handleSignIn,
    );
  }
}
