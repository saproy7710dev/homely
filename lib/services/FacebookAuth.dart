import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class FacebookAuth extends StatefulWidget {
  @override
  _FacebookAuthState createState() => _FacebookAuthState();
}

class _FacebookAuthState extends State<FacebookAuth> {
  handleSignIn() {
    print('Facebook Sign In Button clicked');
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: SvgPicture.asset('lib/assets/icons/facebook.svg'),
      onPressed: handleSignIn,
    );
  }
}
