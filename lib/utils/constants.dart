import 'package:flutter/material.dart';

// Colors
final Color kBackgroundColor = Color(0xFFF5F6F8);
final Color kSelectedColor = Color(0xFF515C6F);
final Color kUnselectedColor = Color(0xFFD4D6DC);
final Color kPrimaryColor = Color(0xFFFF5C63);
final Color kTextColor = Color(0xFFA6ADB7);
final Color kTextColor2 = Color(0xFF4F5C71);
final Color kClothesBottomGradientColor = Color(0xFFFF6B70);
final Color kClothesTopGradientColor = Color(0xFFFFAA31);
final Color kBeautyBottomGradientColor = Color(0xFF60BAFF);
final Color kBeautyTopGradientColor = Color(0xFF00FFF9);
final Color kShoesBottomGradientColor = Color(0xFF00C629);
final Color kShoesTopGradientColor = Color(0xFF9AFF00);
